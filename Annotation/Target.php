<?php


namespace Annotation;


/**
 * Class Target
 * @package Annotation
 */
#[\Attribute(\Attribute::TARGET_CLASS)] class Target extends Attribute
{

}
