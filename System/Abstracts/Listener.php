<?php
declare(strict_types=1);

namespace Snowflake\Abstracts;


use Exception;

/**
 * Class Listener
 * @package Snowflake\Abstracts
 * 监听的名称
 */
abstract class Listener extends Component implements IListener
{


}
