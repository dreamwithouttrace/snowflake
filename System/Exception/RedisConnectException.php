<?php
declare(strict_types=1);


namespace Snowflake\Exception;


/**
 * Class RedisConnectException
 * @package Snowflake\Exception
 */
class RedisConnectException extends \Exception
{

}
