<?php


namespace Snowflake\Abstracts;


interface Kernel
{

	/**
	 * @return array
	 */
	public function getCommands(): array;

}
