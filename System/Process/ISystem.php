<?php
declare(strict_types=1);


namespace Snowflake\Process;


interface ISystem
{

	public function start();

}
