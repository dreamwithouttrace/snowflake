<?php
declare(strict_types=1);


namespace HttpServer\IInterface;


interface Service
{


	public function onInit();

}
