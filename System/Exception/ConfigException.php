<?php
declare(strict_types=1);


namespace Snowflake\Exception;


/**
 * Class ConfigException
 * @package Snowflake\Exception
 */
class ConfigException extends \Exception
{

}
