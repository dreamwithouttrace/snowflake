<?php

declare(strict_types=1);

namespace Snowflake\Exception;

/**
 * Class InitException
 * @package Snowflake\Exception
 */
class InitException extends \Exception
{

}
