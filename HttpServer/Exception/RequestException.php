<?php
declare(strict_types=1);

namespace HttpServer\Exception;


/**
 * Class RequestException
 * @package HttpServer\Exception
 */
class RequestException extends \Exception
{

}
