<?php


namespace Annotation;


interface Porters
{


	/**
	 * @return mixed
	 */
	public function process(): mixed;


}
